<?php

//$date= date("Y-m-d H:i");
$mail = [];
$mail['to'] = 'sinistre@rentanddrop.com,flotte@groupe-aixia.com,gestion@groupe-aixia.com,support@groupe-aixia.com';
//$mail['cc'] = '';
//$mail['cc'][] = '';
$mail['from'] = 'support@groupe-aixia.com';
$mail['from_name'] = 'Support Aixia Systemes';
//$mail['object'] = 'RND Sinistres Contrats En Attente SAV '.$date; 
$mail['object'] = 'RND Sinistres Contrats En Attente SAV';
$mail['pj'] = 'rnd_sinistres_contrats_ea_sav.csv';
$mail['body'] = '';


$mysqli = new mysqli('188.165.234.50', 'readonly', '33hf6E6XSg', 'webloc_rnd_prod');

if ($mysqli->connect_errno) 
{
	throw new exception($dbClient->connect_error);
}

$mysqli->set_charset('latin1');

$sql = "SELECT 
		contrats.num_contrat As 'Num contrat',
		vehicules.immatriculation AS 'Immat',
		(
			SELECT
				CONCAT_WS(' ',type_civilite.tci_desc, IF(type_civilite.est_particulier = 1,CONCAT_WS(' ',utilisateur.nom,utilisateur.prenom),utilisateur.nom_societe))
			FROM utilisateur
			INNER JOIN type_civilite ON type_civilite.id_type_civilite = utilisateur.civilite
			WHERE 
				utilisateur.id_utilisateur = contrats.id_conducteur
			Limit 1
		) AS 'conducteur principal',
		contrats.id_contrat,
		(
			SELECT
				CONCAT_WS(' ',type_civilite.tci_desc, IF(type_civilite.est_particulier = 1,CONCAT_WS(' ',utilisateur.nom,utilisateur.prenom),utilisateur.nom_societe))
			FROM utilisateur
			INNER JOIN type_civilite ON type_civilite.id_type_civilite = utilisateur.civilite
			WHERE 
				utilisateur.id_utilisateur = contrats.id_payable
			Limit 1
		) AS 'payable par',
(
		SELECT
			CONCAT_WS(' ',type_civilite.tci_desc, IF(type_civilite.est_particulier = 1,CONCAT_WS(' ',utilisateur.nom,utilisateur.prenom),utilisateur.nom_societe))
		FROM utilisateur
		INNER JOIN type_civilite ON type_civilite.id_type_civilite = utilisateur.civilite
		WHERE 
			utilisateur.id_utilisateur = contrats.id_locataire
Limit 1
	) AS 'locataire',
mouvements.date_depart AS 'Date Depart',
agence_depart.raison_sociale AS 'Agence Depart',
mouvements.date_retour_reelle AS 'Date Retour',
agence_retour.raison_sociale AS 'Agence Retour' ,

	
		IFNULL
		(
			(
				SELECT 
					revisions.date_revision
				FROM contrats_history
				INNER JOIN revisions ON revisions.id_revision = contrats_history.id_revision
				INNER JOIN types_universal_status ON types_universal_status.id_universal_status = contrats_history.id_universal_status
				
				WHERE 
					contrats_history.id_contrat = contrats.id_contrat
					AND types_universal_status.id_universal_status = 10
				ORDER BY contrats_history.id_revision ASC
				LIMIT 1
			),
			(
				SELECT 
					revisions.date_revision
				FROM revisions
				WHERE
					revisions.id_revision = contrats.id_revision
			)
		) AS 'date EA SAV',

categorie_vehicule.nom_cat AS 'Categorie Veh',

IFNULL
	(
		(
			SELECT
				lignes_facture.franchise_reduite
			FROM lignes_facture
      INNER JOIN produits on produits.id_produit=lignes_facture.id_produit
			WHERE 
				lignes_facture.id_facture = factures.id_facture
				AND produits.id_produit_base in (287414)
				LIMIT 1
		),
		0
	) AS 'Franchise dommages',

(
SELECT date_reglement 
FROM reglements 
where reglements.id_contrat=contrats.id_contrat 
AND 
reglements.id_type_cible_reglement =2
Limit 1
 ) AS 'Date DG',
(
SELECT reglements.montant
FROM reglements 
where reglements.id_contrat=contrats.id_contrat 
AND 
reglements.id_type_cible_reglement =2
Limit 1
 ) AS 'Montant_DG',

if(mouvements.dommage_constate_retour=0,'Non','Oui') AS 'Dommages Retour' ,
IF (contrats.accident is NULL ,'Oui','Non') As 'Accident',

NULL AS 'vide1',
IFNULL

		(
			(SELECT
				 CONVERT(CONCAT( ticket.date_crea,' | ',utilisateur.nom,' ',utilisateur.prenom,' | ',type_ticket.lib_type_ticket,' | ',REPLACE(CONVERT(ticket_message.message USING latin1),'\r\n',' ')) USING latin1) 
			FROM ticket
			INNER JOIN utilisateur on utilisateur.id_utilisateur=ticket.id_utilisateur
      INNER JOIN ticket_message on ticket_message.id_ticket=ticket.id_ticket
      INNER JOIN type_ticket on type_ticket.id_type_ticket= ticket.id_type_ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1
		)
,NULL) AS 'Ticket 1',
NULL AS 'vide2',

/***** Debut TICKET 2**** Avant dernier***/
IFNULL
(
		(
			SELECT
			 CONVERT(CONCAT( ticket.date_crea,' | ',utilisateur.nom,' ',utilisateur.prenom,' | ',type_ticket.lib_type_ticket,' | ',REPLACE(CONVERT(ticket_message.message USING latin1),'\r\n',' ')) USING latin1)  
			FROM ticket
			INNER JOIN utilisateur on utilisateur.id_utilisateur=ticket.id_utilisateur
      INNER JOIN ticket_message on ticket_message.id_ticket=ticket.id_ticket
      INNER JOIN type_ticket on type_ticket.id_type_ticket= ticket.id_type_ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1,1
		)
,NULL) AS 'Ticket 2',

/***** FIN TICKET 2**** Avant dernier ticket***/
NULL AS 'vide3',
/***** Debut TICKET 3**** avant Avant dernier***/
IFNULL
(
		(
			SELECT
				 CONVERT(CONCAT( ticket.date_crea,' | ',utilisateur.nom,' ',utilisateur.prenom,' | ',type_ticket.lib_type_ticket,' | ',REPLACE(CONVERT(ticket_message.message USING latin1),'\r\n',' ')) USING latin1) 
			FROM ticket
			INNER JOIN utilisateur on utilisateur.id_utilisateur=ticket.id_utilisateur
      INNER JOIN ticket_message on ticket_message.id_ticket=ticket.id_ticket
      INNER JOIN type_ticket on type_ticket.id_type_ticket= ticket.id_type_ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 2,1
		)
,NULL) AS 'Ticket 3'
/***** FIN TICKET 3****dernier ticket***/

FROM 
contrats
INNER JOIN mouvements ON mouvements.id_mouvement = contrats.id_mouvement
INNER JOIN vehicules on vehicules.id_vehicule = mouvements.id_vehicule
INNER JOIN societes AS agence_depart ON agence_depart.id_societe = mouvements.id_agence_depart
INNER JOIN societes AS agence_retour ON agence_retour.id_societe = mouvements.id_agence_retour
INNER JOIN utilisateur AS payable ON payable.id_utilisateur = contrats.id_payable
INNER JOIN type_civilite ON type_civilite.id_type_civilite =  payable.civilite
INNER JOIN types_etat_contrat AS tc on tc.id_etat_contrat= contrats.id_etat_contrat
INNER JOIN categorie_vehicule on categorie_vehicule.id_cat_vehi=mouvements.id_cat_vehi
INNER JOIN types_universal_status on types_universal_status.id_universal_status=contrats.id_universal_status
LEFT JOIN reglements on reglements.id_contrat=contrats.id_contrat
LEFT JOIN factures on factures.id_contrat=contrats.id_contrat


where contrats.id_universal_status =10
Group by contrats.id_contrat
ORDER BY Montant_DG ASC";

$result = $mysqli->query($sql);

if($result === false)
{
	throw new \Exception($mysqli->error, $mysqli->errno);
}
$csv = '';
$first = true;
while($row = $result->fetch_assoc()) 
{
	if($first == true)
	{
		$csv = implode(';', array_keys($row)) . PHP_EOL;
		$first = false;
	}
	$csv .= implode(';', $row) . PHP_EOL;
}

$result->close();
$mysqli->close();

$CRLF = "\r\n";
$boundary = '----=_' . md5(uniqid(rand())); 

$headers = array();
$headers[] = 'From: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';
$headers[] = 'Reply-To: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';

$headers[] = 'Subject: ' . $mail['object'];
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-Type: multipart/mixed;';
$headers[] = '	boundary="' . $boundary . '"' . $CRLF . $CRLF;

$message = array();
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/plain; charset="UTF8"';
$message[] = 'Content-Transfer-Encoding: quoted-printable' . $CRLF;
$message[] =  $mail['body'] . $CRLF;
//piece jointe
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/csv; name="' . $mail['pj'] . '"';
$message[] = 'Content-Transfer-Encoding: base64';
$message[] = 'Content-Disposition: attachment; filename="' . $mail['pj'] . '"' . $CRLF;
$message[] = chunk_split(base64_encode($csv));
$message[] = '--' . $boundary . '--';
	
$mail = mail($mail['to'], $mail['object'], implode($CRLF, $message), implode($CRLF, $headers),'-f' . $mail['from']);
