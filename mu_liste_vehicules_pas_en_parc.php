<?php

$date= date("Y-m-d");
$mail = [];
$mail['to'] = 'jose.thobie@mpsa.com,eric.beretti@mpsa.com,kathleen.raffier@mpsa.com,francoisxavier.desoras@citroen.com,leila.zeriahene@mpsa.com,support@groupe-aixia.com';
//$mail['cc'] = '';
//$mail['cc'][] = '';
$mail['from'] = 'support@groupe-aixia.com';
$mail['from_name'] = 'Support Aixia Systemes';
$mail['object'] = 'Etat des vÃ©hicules en statut Pas en parc au '.$date; 

$mail['pj'] = 'Peugeot Rent - Liste des vehicules avec statut pas en parc.csv';
$mail['body'] = '

Bonjour,

Veuillez trouver en piÃ¨ce jointe la liste des vÃ©hicules saisis dans lâ€™application WL Peugeot Rent en statut Â« Pas en Parc Â» . 



Bien Ã  vous.


Peugeot Rent  assistance technique
Help-desk WebAixia

Tel : 0 826 966 491
peugeotrent@groupe-aixia.com

';


$mysqli = new mysqli('192.168.11.12', 'readonly', '33hf6E6XSg', 'webloc_mu_prod');

if ($mysqli->connect_errno) 
{
	throw new exception($dbClient->connect_error);
}

$mysqli->set_charset('latin1');

$sql = "SELECT 
*
FROM
(
                SELECT
                               societes.raison_sociale AS 'SociÃ©tÃ©',
                               agence.raison_sociale AS 'Agence Mise en parc',
                               agence.code_societe AS 'Code SocietÃ©',
                               vehicules.immatriculation AS 'Immatriculation',
                               vehicules.numero_serie AS 'VIN',
                               vehicules.date_immatriculation AS 'Date immatriculation',
                               vehicules.date_entree_prevue AS 'Date EntrÃ©e PrÃ©vue',
                               vehicules.date_entree_reelle AS 'Date EntrÃ©e RÃ©elle',
                               vehicules.id_vehicule As 'id vehicule AIXIA',
                               vehicules.date_crea AS 'date creation',
                               (
                                               SELECT 
                                                               count(id_mouvement)
                                               FROM mouvements
                                               WHERE 
                                                               mouvements.id_vehicule = vehicules.id_vehicule
                               ) AS 'Nombre de mouvement'

                FROM vehicules
                INNER JOIN societes AS agence ON agence.id_societe = vehicules.id_societe
                INNER JOIN societes ON societes.id_societe = agence.id_societe_mere
                WHERE
                               vehicules.activer = 1
                               AND vehicules.deleted = 0
                               AND vehicules.date_entree_reelle IS NULL
                               AND societes.id_societe NOT IN (SELECT id_societe FROM templates WHERE id_type_template = 2)
                               AND agence.id_societe NOT IN (SELECT id_societe FROM templates WHERE id_type_template = 2)
) AS vehicules

ORDER BY vehicules.`Nombre de mouvement` DESC";

$result = $mysqli->query($sql);

if($result === false)
{
	throw new \Exception($mysqli->error, $mysqli->errno);
}
$csv = '';
$first = true;
while($row = $result->fetch_assoc()) 
{
	if($first == true)
	{
		$csv = implode(';', array_keys($row)) . PHP_EOL;
		$first = false;
	}
	$csv .= implode(';', $row) . PHP_EOL;
}

$result->close();
$mysqli->close();

$CRLF = "\r\n";
$boundary = '----=_' . md5(uniqid(rand())); 

$headers = array();
$headers[] = 'From: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';
$headers[] = 'Reply-To: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';

$headers[] = 'Subject: ' . $mail['object'];
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-Type: multipart/mixed;';
$headers[] = '	boundary="' . $boundary . '"' . $CRLF . $CRLF;

$message = array();
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/plain; charset="UTF8"';
$message[] = 'Content-Transfer-Encoding: quoted-printable' . $CRLF;
$message[] =  $mail['body'] . $CRLF;
//piece jointe
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/csv; name="' . $mail['pj'] . '"';
$message[] = 'Content-Transfer-Encoding: base64';
$message[] = 'Content-Disposition: attachment; filename="' . $mail['pj'] . '"' . $CRLF;
$message[] = chunk_split(base64_encode($csv));
$message[] = '--' . $boundary . '--';
	
$mail = mail($mail['to'], $mail['object'], implode($CRLF, $message), implode($CRLF, $headers),'-f' . $mail['from']);
