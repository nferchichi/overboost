<?php
error_reporting(E_ALL & ~E_NOTICE);
$profile = 
[
	'convoyeur',
	'ne_pas_relancer_satisfaction'
];

$interval = 3;

/*if (date('N', strtotime('now')) == 1) {
	$interval = 3;
}*/

$mail = [];
$mail['to'] = 'nsalmani@groupe-aixia.com,mneili@groupe-aixia.com,jsvetois@groupe-aixia.com,calltunis@groupe-aixia.com,gestion@groupe-aixia.com,support@groupe-aixia.com';
//$mail['cc'] = 'hnaouali@groupe-aixia.com';
//$mail['cc'][] = 'fducoing@groupe-aixia.com,support@groupe-aixia.com,superviseurs@groupe-aixia.com';

$mail['from'] = 'support@groupe-aixia.com';
$mail['from_name'] = 'Support Aixia Systemes';
$mail['object'] = '[RND] Relance DG'; 
$mail['pj'] = 'relance_dg.csv';
$mail['body'] = 'Madame, Monsieur,

Vous trouverez en PJ la liste des departs de J+3 pour les relances DG  .

Cordialement,

Equipe support Aixia Systemes

Groupe AIXIA
Tel : 04.78.14.09.55
Mail : support@groupe-aixia.com';


$mysqli = new mysqli('188.165.234.50', 'readonly', '33hf6E6XSg', 'webloc_rnd_prod');

if ($mysqli->connect_errno) 
{
	throw new exception($dbClient->connect_error);
}

$mysqli->set_charset('latin1');

$sql = "SELECT
								'RND RELANCES DG' AS 'Campagne',

                contrats.num_contrat AS 'numero contrat',

                mouvements.date_depart AS 'date depart',

                mouvements.date_retour AS 'date de retour',

                agence_depart.raison_sociale AS 'agence depart',

                agence_retour.raison_sociale AS 'agence de retour',
								categorie_vehicule.nom_cat AS 'Categorie',
				(
						SELECT
							CONCAT_WS(' ',type_civilite.tci_lib, IF(type_civilite.est_particulier = 1,CONCAT_WS(' ',utilisateur.nom,utilisateur.prenom),utilisateur.nom_societe))
						FROM utilisateur
						INNER JOIN type_civilite ON type_civilite.id_type_civilite = utilisateur.civilite
						WHERE 
							utilisateur.id_utilisateur = contrats.id_payable
					) AS 'payable par',

                CASE

                               WHEN locataire_civilite.est_particulier = 1 THEN CONCAT(locataire_civilite.tci_lib, ' ', locataire.nom, ' ', locataire.prenom)

                               WHEN locataire_civilite.est_particulier = 0 THEN CONCAT(locataire_civilite.tci_lib, ' ', locataire.nom_societe)

                END AS 'locataire',
				  CASE

                               WHEN conducteur_civilite.est_particulier = 1 THEN CONCAT(conducteur_civilite.tci_lib, ' ', conducteur.nom, ' ', conducteur.prenom)

                               WHEN conducteur_civilite.est_particulier = 0 THEN CONCAT(conducteur_civilite.tci_lib, ' ', conducteur.nom_societe)

                END AS 'conducteur',

                locataire.tel_mobile AS 'numero telephone locataire',

                locataire.email 'email du locataire',

              

                lignes_facture.franchise_reduite AS 'montant DG',

                CASE

                               WHEN contrats.dg_paye = 0 THEN 'Non'

                               WHEN contrats.dg_paye = 1 THEN 'Oui'

                END AS 'DG paye',
NULL AS 'Vide1',
contrats.id_contrat,
	IFNULL
		(
			(
				SELECT 
					revisions.date_revision
				FROM contrats_history
				INNER JOIN revisions ON revisions.id_revision = contrats_history.id_revision
				INNER JOIN types_universal_status ON types_universal_status.id_universal_status = contrats_history.id_universal_status
				
				WHERE 
					contrats_history.id_contrat = contrats.id_contrat
					AND types_universal_status.id_universal_status in (4)
				ORDER BY contrats_history.id_revision ASC
				LIMIT 1
			),
			(
				SELECT 
					revisions.date_revision
				FROM revisions
				WHERE
					revisions.id_revision = contrats.id_revision 
        
			)
		) AS 'date_confirmation',
NULL AS 'Vide2',

/***** Debut TICKET 1**** dernier ticket***/
IFNULL
(
		(
			SELECT
				ticket.date_crea 
			FROM ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1
		)
,NULL) AS 'Date Ticket 1',
IFNULL
(
		(
			SELECT
				ticket.objet
			FROM ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1
		)
,NULL) AS 'Objet Ticket 1',
IFNULL
(
		REPLACE((
			SELECT
				CONVERT(ticket_message.message USING latin1)
			FROM ticket
      INNER JOIN ticket_message on ticket_message.id_ticket=ticket.id_ticket
   			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
        ORDER BY ticket.id_ticket DESC
			LIMIT 1
		),'\r\n',' ')
,NULL) AS 'Message Ticket 1',
IFNULL
(
		(
			SELECT
				CONCAT_WS(' ', utilisateur.nom, utilisateur.prenom)
			FROM ticket
      INNER JOIN utilisateur on utilisateur.id_utilisateur=ticket.id_utilisateur
   			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
        ORDER BY ticket.id_ticket DESC
			LIMIT 1
		)
,NULL) AS 'Auteur Ticket 1',



/***** FIN TICKET 1**** dernier ticket***/
NULL AS 'Vide3',

/***** Debut TICKET 2**** Avant dernier***/
IFNULL(
(
			SELECT
				ticket.date_crea 
			FROM ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1,1
		) 
, NULL) AS 'Date ticket 2',
IFNULL
(
		(
			SELECT
				ticket.objet
			FROM ticket
			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
			ORDER BY ticket.id_ticket DESC
			LIMIT 1,1
		)
,NULL) AS 'Objet Ticket 2',
IFNULL
(
		REPLACE((
			SELECT
				CONVERT(ticket_message.message USING latin1)
			FROM ticket
      INNER JOIN ticket_message on ticket_message.id_ticket=ticket.id_ticket
   			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
        ORDER BY ticket.id_ticket DESC
			LIMIT 1,1
		),'\r\n',' ')
,NULL) AS 'Message Ticket 2',
IFNULL
(
		(
			SELECT
				CONCAT_WS(' ', utilisateur.nom, utilisateur.prenom)
			FROM ticket
      INNER JOIN utilisateur on utilisateur.id_utilisateur=ticket.id_utilisateur
   			WHERE
				ticket.type_document_lie = 1
				AND ticket.document_lie = contrats.id_contrat
        ORDER BY ticket.id_ticket DESC
			LIMIT 1,1
		)
,NULL) AS 'Auteur Ticket 2'

/***** FIN TICKET 2**** Avant dernier ticket***/

FROM contrats

INNER JOIN mouvements ON mouvements.id_mouvement = contrats.id_mouvement
INNER JOIN categorie_vehicule on categorie_vehicule.id_cat_vehi=contrats.id_categorie_location

INNER JOIN societes AS agence_depart ON agence_depart.id_societe = mouvements.id_agence_depart

INNER JOIN societes AS agence_retour ON agence_retour.id_societe = mouvements.id_agence_retour

INNER JOIN utilisateur AS locataire ON
(
                locataire.id_utilisateur = contrats.id_locataire
                AND locataire.id_utilisateur NOT IN 
                (
                               SELECT
                                               utilisateur_has_profile.id_utilisateur
                               FROM utilisateur_has_profile
                               WHERE
                                               utilisateur_has_profile.id_profile IN (@profile_exclu) 
                )
)

INNER JOIN type_civilite AS locataire_civilite ON locataire_civilite.id_type_civilite = locataire.civilite
INNER JOIN utilisateur AS conducteur ON
(
                conducteur.id_utilisateur = contrats.id_conducteur
                AND conducteur.id_utilisateur NOT IN 
                (
                               SELECT
                                               utilisateur_has_profile.id_utilisateur
                               FROM utilisateur_has_profile
                               WHERE
                                              utilisateur_has_profile.id_profile IN (SELECT GROUP_CONCAT(id_profile) FROM `profile` WHERE lib IN(" . "'" . implode("','", $profile) . "'" . "))
                )
)

INNER JOIN type_civilite AS conducteur_civilite ON conducteur_civilite.id_type_civilite = conducteur.civilite


INNER JOIN factures ON

(

                factures.id_contrat = contrats.id_contrat

                AND factures.id_statut_facture = 4

)

INNER JOIN lignes_facture ON

(

                lignes_facture.id_facture = factures.id_facture

                AND lignes_facture.lib_produit = 'DEPOT GARANTIE'

)

WHERE

                contrats.id_etat_contrat = 2

                AND contrats.id_universal_status = 4

              AND DATE(date_depart) = DATE_ADD(DATE(NOW()),INTERVAL " . $interval . " day)
				AND  agence_retour.raison_sociale NOT LIKE '%test%' 
				AND	 agence_depart.raison_sociale NOT LIKE '%test%'

ORDER BY lignes_facture.franchise_reduite DESC, mouvements.date_depart ASC";

$result = $mysqli->query($sql);

if($result === false)
{
	throw new \Exception($mysqli->error, $mysqli->errno);
}
$csv = '';
$first = true;
while($row = $result->fetch_assoc()) 
{
	if($first == true)
	{
		$csv = implode(';', array_keys($row)) . PHP_EOL;
		$first = false;
	}
	$csv .= implode(';', $row) . PHP_EOL;
}

$result->close();
$mysqli->close();

$CRLF = "\r\n";
$boundary = '----=_' . md5(uniqid(rand())); 

$headers = array();
$headers[] = 'From: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';
$headers[] = 'Reply-To: "' . $mail['from_name'] . '"<' . $mail['from'] . '>';

$headers[] = 'Subject: ' . $mail['object'];
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-Type: multipart/mixed;';
$headers[] = '	boundary="' . $boundary . '"' . $CRLF . $CRLF;

$message = array();
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/plain; charset="UTF8"';
$message[] = 'Content-Transfer-Encoding: quoted-printable' . $CRLF;
$message[] =  $mail['body'] . $CRLF;
//piece jointe
$message[] = '--' . $boundary;
$message[] = 'Content-Type: text/csv; name="' . $mail['pj'] . '"';
$message[] = 'Content-Transfer-Encoding: base64';
$message[] = 'Content-Disposition: attachment; filename="' . $mail['pj'] . '"' . $CRLF;
$message[] = chunk_split(base64_encode($csv));
$message[] = '--' . $boundary . '--';
	
$mail = mail($mail['to'], $mail['object'], implode($CRLF, $message), implode($CRLF, $headers),'-f' . $mail['from']);
